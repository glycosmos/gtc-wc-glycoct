import { LitElement, html } from 'lit-element';
import 'lit-element-bootstrap';
import getdomain from 'gly-domain/gly-domain.js';

class GtcWcGlycoCT extends LitElement {
  static get properties() {
    return {
      accession: String,
      sampleids: Array,
      sparqlistUrl: String
    };
  }

  constructor() {
    super();
    console.log("constructor");
    this.accession="G54245YQ";
    this.sampleids=[];
    this.sparqlistUrl="";
  }

  render() {
    return html `
      <style>
        .nothingFound {
          width: 100%;
          float: none;
          margin: 0 0 10px;
          padding: 5px 0;
          background: #EEE;
          color: #999;
          font-size: 14px;
          font-weight: bold;
          text-align: center;
          box-sizing: border-box;
        }
      </style>
      <div>${this._processHtml()}</div>
    `;
  }

  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
    const host =  getdomain(location.href);
    this.sparqlistUrl = "https://" + host + "/sparqlist/api/";
    const url1 = this.sparqlistUrl + 'gtc_glycoct?accNum=' + this.accession;
    this.getContents(url1);
  }

  getContents(url1) {
    console.log(url1);
    var urls = [];
    urls.push(url1);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("summary");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));
    Promise.all(promises).then(results => {
      console.log("values");
      console.log(results);
      this.sampleids = results.pop();
    });
  }

  _processHtml() {
    if (this.sampleids.length > 0) {
      // console.log(Object.keys(this.sampleids[0]));
      const sequenceContents = this.sampleids.map(item => {
        return html`
          <pre>${item.GlycoCT}</pre>
        `;
      });
      return sequenceContents;
    } else {
      return html`<div class="nothingFound">Nothing found</div>`;
    }
  }

}

customElements.define('gtc-wc-glycoct', GtcWcGlycoCT);
